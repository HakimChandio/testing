CREATE TABLE [dbo].[USER_CI_DATA]
(
[FSA_Sno] [bigint] NOT NULL IDENTITY(1, 1),
[Group_Name] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Customer_No] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Short_Name] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Accountno] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ExposureFlag] [char] (2) COLLATE Latin1_General_CI_AS NULL,
[ProductFlag] [char] (2) COLLATE Latin1_General_CI_AS NULL,
[NonAccrual] [char] (2) COLLATE Latin1_General_CI_AS NULL,
[Balance] [decimal] (20, 4) NOT NULL,
[Base_Balance] [decimal] (20, 4) NOT NULL,
[Cust_Type] [char] (2) COLLATE Latin1_General_CI_AS NULL,
[Acc_Type] [char] (2) COLLATE Latin1_General_CI_AS NULL,
[Cty_res] [char] (2) COLLATE Latin1_General_CI_AS NULL,
[Curr_Code] [char] (3) COLLATE Latin1_General_CI_AS NULL,
[Cty_Risk] [char] (2) COLLATE Latin1_General_CI_AS NULL,
[Mat_Date] [date] NULL,
[Entity] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[InterestRate] [decimal] (20, 4) NULL,
[FSA_SystemDate] [date] NOT NULL,
[BookValue] [decimal] (20, 4) NULL,
[MarketValue] [decimal] (20, 4) NULL,
[Country_ID] [int] NULL,
[Provision] [decimal] (20, 4) NULL,
[Group_Def_ID] [int] NULL,
[Group_ID] [int] NULL,
[InvestmentPrice_ID] [bigint] NULL,
[Limit_FI_ID] [int] NULL,
[Collateral_ID] [int] NULL,
[Limit_Corporate_ID] [int] NULL,
[Limit_OD_ID] [int] NULL,
[DrawDownDate] [date] NULL,
[Start_Date] [datetime] NULL,
[BranchCode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL
) ON [ps_RR_User_CI_Data_Stage2] ([FSA_SystemDate])
GO
ALTER TABLE [dbo].[USER_CI_DATA] ADD CONSTRAINT [PK_USER_CI_DATA] PRIMARY KEY CLUSTERED  ([FSA_Sno], [FSA_SystemDate]) ON [ps_RR_User_CI_Data_Stage2] ([FSA_SystemDate])
GO

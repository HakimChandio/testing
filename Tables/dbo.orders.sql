CREATE TABLE [dbo].[orders]
(
[order_id] [bigint] NOT NULL IDENTITY(1, 1),
[user_id] [bigint] NULL,
[order_amt] [decimal] (10, 2) NULL,
[address_id] [bigint] NULL,
[status_id] [tinyint] NULL,
[is_active] [bit] NULL,
[order_date] [datetime] NULL
) ON [PS_MonthWise] ([order_date])
GO
CREATE CLUSTERED INDEX [CI_orders_order_id] ON [dbo].[orders] ([order_id]) ON [PS_MonthWise] ([order_date])
GO
CREATE NONCLUSTERED INDEX [IX_user_id] ON [dbo].[orders] ([user_id]) ON [PS_MonthWise] ([order_date])
GO

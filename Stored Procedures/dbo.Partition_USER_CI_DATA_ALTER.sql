SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[Partition_USER_CI_DATA_ALTER]
AS
BEGIN

-- EXEC Partition_USER_CI_DATA_ALTER

					DECLARE @DBName NVARCHAR(200) = 'PartitionDB'
					DECLARE @TableName NVARCHAR(200) = 'USER_CI_DATA'

					IF EXISTS(SELECT [name] FROM tempdb.sys.tables WHERE [name] = '#temp') 
					BEGIN
					   DROP TABLE #temp;
					END;
					
					IF EXISTS(SELECT [name] FROM tempdb.sys.tables WHERE [name] = '#generateScript') 
					BEGIN
					   DROP TABLE #generateScript;
					END;




				SELECT o.NAME                          AS table_name,
				       pf.NAME                         AS PartitionFunction,
				       ps.NAME                         AS PartitionScheme,
				       Max(Cast(rv.value AS DATETIME)) AS LastPartitionRange,
				       CASE
				         WHEN Max(rv.value) <= Dateadd(month, 2, Getdate()) THEN 1
				         ELSE 0
				       END                             AS isRequiredMaintenance
				INTO   #temp
				FROM   sys.partitions p
				       INNER JOIN sys.indexes i
				               ON p.object_id = i.object_id
				                  AND p.index_id = i.index_id
				       INNER JOIN sys.objects o
				               ON p.object_id = o.object_id
				       INNER JOIN sys.system_internals_allocation_units au
				               ON p.partition_id = au.container_id
				       INNER JOIN sys.partition_schemes ps
				               ON ps.data_space_id = i.data_space_id
				       INNER JOIN sys.partition_functions pf
				               ON pf.function_id = ps.function_id
				       INNER JOIN sys.partition_range_values rv
				               ON pf.function_id = rv.function_id
				                  AND p.partition_number = rv.boundary_id
				WHERE o.NAME   = @TableName
				GROUP  BY o.NAME,
				          pf.NAME,
				          ps.NAME 


					SELECT table_name,
					       partitionfunction,
					       partitionscheme,
					       lastpartitionrange,
					       Cast(Dateadd(month, 1, lastpartitionrange) AS DATE)              AS NewRange,
					       'FG_'+@DBName+'_'+ Cast(Format(Dateadd(month, 1, lastpartitionrange), 'dd_MMM_yyyy') AS NVARCHAR(200))  AS NewFileGroup,
					       'FG_'+@DBName+'_'+ Cast(Format(Dateadd(month, 1, lastpartitionrange), 'ddMMMyyyy') AS NVARCHAR(200))   AS FileName,
					       'E:\Data\Microsoft SQL Server\MSSQL15.SQLSERVER2019\MSSQL\DATA\' AS file_path
					INTO   #generatescript
					FROM   #temp
					WHERE  isrequiredmaintenance = 1 

					--SELECT * FROM #generatescript

					DECLARE @filegroup NVARCHAR(MAX) = ''
					DECLARE @file NVARCHAR(MAX) = ''
					DECLARE @PScheme NVARCHAR(MAX) = ''
					DECLARE @PFunction NVARCHAR(MAX) = ''
 
					SELECT @filegroup = @filegroup + 
					    CONCAT('IF NOT EXISTS(SELECT 1 FROM '+@DBName+'.sys.filegroups WHERE name = ''',NewFileGroup,''')
					    BEGIN
					      ALTER DATABASE '+@DBName+' ADD FileGroup ',NewFileGroup,' 
					    END;'),
					    @file = @file + CONCAT('IF NOT EXISTS(SELECT 1 FROM '+@DBName+'.sys.database_files WHERE name = ''',FileName,''')
					    BEGIN
					    ALTER DATABASE '+@DBName+' ADD FILE 
					    (NAME = ''',FileName,''', 
					    FILENAME = ''',File_Path,FileName,'.ndf'', 
					    SIZE = 5MB, MAXSIZE = UNLIMITED, 
					    FILEGROWTH = 10MB )
					    TO FILEGROUP ',NewFileGroup, '
					    END;'),
					    @PScheme = @PScheme + CONCAT('ALTER PARTITION SCHEME ', PartitionScheme, ' NEXT USED ',NewFileGroup,';'),
					    @PFunction = @PFunction + CONCAT('ALTER PARTITION FUNCTION ', PartitionFunction, '() SPLIT RANGE (''',NewRange,''');')
					FROM #generateScript
 
					EXEC (@filegroup)
					EXEC (@file)
					EXEC (@PScheme)
					EXEC (@PFunction)
END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE proc [dbo].[Partition_USER_CI_DATA]
as
begin
--exec  Partition_USER_CI_DATA
	

	DECLARE @DBName NVARCHAR(200) = 'PartitionDB'
	DECLARE @TableName  NVARCHAR(200) = 'USER_CI_DATA'
	DECLARE @PartitionFunctionName AS NVARCHAR(50) = 'pf_RR_User_CI_Data_Stage2';
	DECLARE @PartitionSchemaName   AS NVARCHAR(50) = 'ps_RR_User_CI_Data_Stage2';

	IF EXISTS(SELECT [name] FROM tempdb.sys.tables WHERE [name] = '#tempPartition') 
	BEGIN
	   DROP TABLE #tempPartition;
	END;
	
	IF EXISTS(SELECT [name] FROM tempdb.sys.tables WHERE [name] = '#tempPartition1') 
	BEGIN
	   DROP TABLE #tempPartition1;
	END;


	DECLARE @Counter INT 
		  , @MaxId INT
		  , @FGFSA_SystemDate NVARCHAR(MAX) 
		  , @FileName NVARCHAR(MAX)
		  , @FSA_SystemDate NVARCHAR(MAX) 
		  , @DropConstraints NVARCHAR(MAX)
		  , @AddConstraints NVARCHAR(MAX)

		SELECT DISTINCT  EOMONTH(FSA_SYSTEMDATE)  FSA_SYSTEMDATE  
		INTO #TEMPPARTITION 
		FROM USER_CI_DATA 
		WHERE FSA_SYSTEMDATE IS NOT NULL
		ORDER BY EOMONTH(FSA_SYSTEMDATE)  ASC 
		
		SELECT top 3 ROW_NUMBER() OVER ( ORDER BY FSA_SYSTEMDATE )  SNO,
								EOMONTH(FSA_SYSTEMDATE)  FSA_SYSTEMDATE  
		INTO #TEMPPARTITION1  
		FROM #TEMPPARTITION 

		SELECT @Counter = min(SNo) , @MaxId = max(SNo) 
		FROM #TEMPPARTITION1

		DECLARE @PartitionFiles NVARCHAR(MAX) = ''
		DECLARE @PartitionFilesSystemDate NVARCHAR(MAX) = ''

		WHILE(@Counter IS NOT NULL AND @Counter <= @MaxId)
		BEGIN
				 SELECT @FGFSA_SystemDate = 'FG_PartitionDB_'+format(fsa_systemdate,'dd_MMM_yyyy') 
				      , @FileName		  = 'File_PartitionDB_'+format(fsa_systemdate,'ddMMMyyyy') , @FSA_SystemDate = FSA_SYSTEMDATE  FROM #tempPartition1 WHERE SNo = @Counter
				 
				 
				 DECLARE @filegroup NVARCHAR(MAX) = ''
				 DECLARE @filegroupAdd NVARCHAR(MAX) = ''
				 DECLARE @PartitionSchema NVARCHAR(MAX) = ''
				 DECLARE @PartitionFunction NVARCHAR(MAX) = ''

				 SET @PartitionFiles += ''''+@FGFSA_SystemDate +''' , ';
				 SET @PartitionFilesSystemDate += ''''+@FSA_SystemDate +''' , ';

				 IF(@Counter = @MaxId)
				 BEGIN
					SET @PartitionFiles += '''Primary''';
					
					--Removed last comma
					set @PartitionFilesSystemDate = Left(@PartitionFilesSystemDate,len(@PartitionFilesSystemDate)-1)

				 END

				 SELECT @filegroup = 'IF NOT EXISTS(SELECT 1 FROM '+@DBName+'.sys.filegroups WHERE name = '''+@FGFSA_SystemDate+''')
																BEGIN
																	ALTER DATABASE '+@DBName+' ADD FileGroup '+@FGFSA_SystemDate+' 
																END;';
						
														DECLARE @file NVARCHAR(MAX) = ''
														SELECT	@file = 'IF NOT EXISTS(SELECT 1 FROM '+@DBName+'.sys.database_files WHERE name = '''+@FileName+''')
															BEGIN
															ALTER DATABASE '+@DBName+' ADD FILE 
															(NAME = '''+@FileName+''', 
															FILENAME = ''E:\Data\Microsoft SQL Server\MSSQL15.SQLSERVER2019\MSSQL\DATA\'+@FileName+'.ndf'', 
															SIZE = 5MB, MAXSIZE = UNLIMITED, 
															FILEGROWTH = 10MB )
															TO FILEGROUP '+@FGFSA_SystemDate+ '
															END;';


				exec (@filegroup)
				exec (@file)
					
				SET @Counter  = @Counter  + 1   
				    
END
			
				SELECT @PartitionFunction = N'CREATE PARTITION FUNCTION '+@PartitionFunctionName+'(DATE) AS RANGE LEFT FOR VALUES ('+@PartitionFilesSystemDate+')' ;
				EXEC  (@PartitionFunction)

				
				SELECT @PartitionSchema = 'CREATE PARTITION SCHEME '+@PartitionSchemaName+' AS PARTITION '+@PartitionFunctionName+' TO ( '+@PartitionFiles+' );'
				EXEC  (@PartitionSchema)



				IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'PK_USER_CI_DATA'))
					BEGIN
					SELECT @DropConstraints = 'ALTER TABLE '+@TableName+'  DROP CONSTRAINT PK_USER_CI_DATA';
					EXEC  (@DropConstraints)
					END
					
				


				SELECT @AddConstraints = 
				'ALTER TABLE  '+@TableName+' add CONSTRAINT PK_USER_CI_DATA PRIMARY KEY CLUSTERED ( fsa_sno, fsa_systemdate ) 
				WITH(	PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF,IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
				ON '+@PartitionSchemaName+'(fsa_systemdate)';
				EXEC  (@AddConstraints)
END




GO
